---
title: "Slow Cooker Curry"
description: "Use any curry paste you want, I like Nyonya."
summary: "Use any curry paste you want, I like Nyonya."
date: "2021-10-16T19:38:39"
category: "recipes"
---

1. Brown chicken drumsticks in a pan, you don't need to cook it through, move to slow cooker.
   - **Chicken Drumsticks**, 8
2. Cut onion into thin slices, saute in pan, when soft add curry paste, coconut milk, stock, lemongrass, cinnamon, star anise, lime leaves. Simmer until fragrant. Then Add to slow cooker.
   - **Onion**, 1 -- Sliced
   - **Curry Paste**, 1 Jar
   - **Coconut Milk**, 1 Can
   - **Chicken Stock**, 1/2 Cup
   - **Lemongrass**, 1 Stalk
   - **Cinnamon**, 1 Stick
   - **Star Anise**, 1
   - **Kaffir Lime Leaves**, 2
3. Peel potatoes, quarter and add to slow cooker.
   - **Potatoes**, 500g
4. Slow cook on low for 4 hours.
5. Add sugar and fish sauce and stir.
   - **Brown Sugar**, 2 tsp
   - **Fish Sauce**, 2 tsp
