---
title: "Spring Onion and Niboshi Oil"
description: "A fairly versatile fishy ramen oil."
summary: "A fairly versatile fishy ramen oil."
date: "2022-09-25T22:50:17"
series:
  - "ramen"
category: "recipes"
---

1. Cut spring onion into 4cm pieces, discard ends.
   - **Spring Onions**, 5
2. Add all ingredients into a small pan at medium low heat.
   - **Spring Onion**
   - **Chicken Fat**, 130g
   - **Niboshi**, 10
3. Saute until the spring onion and niboshi are a light golden colour and the the oil is fragrant.
4. Strain and transfer to a container.
