---
title: "Chashu Pork"
description: "Primarily for ramen, but it could work on a sandwich."
summary: "Primarily for ramen, but it could work on a sandwich."
date: "2022-09-30T09:49:15"
category: "recipes"
series:
  - "ramen"
---

## Ingredients

| Ingredient      | Amount             |
| :-------------- | :----------------- |
| Pork belly      | Any amount         |
| Salt and Pepper | To Taste           |
| Water           | 250ml              |
| Soy Sauce       | 125ml              |
| Mirin           | 125ml              |
| Sugar           | 12g                |
| Garlic          | 3 cloves (crushed) |
| Sake            | 60ml               |

## Preparation

> Preheat oven to **_110C°C_**.

1. Roll up pork belly, season with a bit of salt and pepper, tie with twine.
   - **Pork belly** -- Any amount
   - **Salt and Pepper**, to taste
2. In a cast iron pot sear all sides of the pork belly.
3. In a bowl mix water, soy sauce, mirin, sugar, garlic, add to pot.
   - **Water**, 250ml
   - **Soy Sauce**, 125ml
   - **Mirin**, 125ml
   - **Sugar**, 12g
   - **Garlic**, 3 Cloves -- Crushed
4. Deglaze with sake.
   - **Sake**, 60ml
5. Cover the pot bring liquid to a boil.
6. Place pot in the oven with a temperature probe. Turn pork every hour or so. Remove when internal temperature reaches **_93°C_**.
7. Allow to cool to room temperature.
8. Slice pork then serve.
