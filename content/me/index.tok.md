---
title: "tan mi"
description: "Who is Sebastiaan?"
date: "2021-07-15T10:06:14"
aliases:
  - "/about"
category: "me"
---

toki nimi mi li Sepaten -- jan pi pona tawa nanpa wan pi lipu. sina pona tan lukin 👋

Hi I'm Sebastiaan -- the protagonist of this website. Thanks for dropping on by 👋

I originally intended to write [blog posts](/blog) about DevOps, technology, and other various topics that caught my interest. For the most part that hasn't happened. Instead the bulk of the posts now are [guides](/guides) and [recipes](/recipes) I'll refer to now and then.

The site has been a great learning experience for both web development and writing. But as you can see there's still quite a lot to be done!

Related to that: my occupation. I've been a DevOps Engineer for the past {{% years-since 2018 %}} years. Mostly working with AWS, Terraform, Docker, Jenkins. At the moment I work for [Secure Code Warrior](https://www.securecodewarrior.com) a gamified cybersecurity education company based in Sydney, Australia.

All the posts on this site reflect my personal views, and do not reflect the opinions of my current or previous employers.

## Contact Information

If would like to get in contact, the best way is probably via email at [t@svw.au](mailto:t@svw.au).

If PGP means anything to you, you can import my key via `gpg --locate-keys t@svw.au` ([see how that's done here](/guides/gpg-wkd-on-a-static-site)). Alternatively [here is my public key](/documents/svw.asc).

<details><summary>PGP Key</summary>

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGCR738BDACBRK7pcZgkyNVAvubjY4LOTqFo1Yvy2oMcU/zGib2FYDMmxbSH
xRKbpg0eDtIWbFVyypoZyFZTP75SM5Xjio6njiu8T8LLrmui339WM2+JJ+Y1fc06
3KX1GQc2eJEdjJr3QLtO4/WhpIwTy9fA578grtLdnppTTPa0p+9XFP5pIqk7waa8
/9luI8m0+grbHwILRmmE6E/sToBXKC5lTeTbUZewWIqdil+58uYNrGnHBRo5/Hy5
+IRQHF7VK0Svbnl6k8aDpxqJljsCzPGcFSZrufialxlVAFfQoS61qZPKNZvXhxPB
WZe+Y2PBL6jJH0Ck6C34dXJ4LDzY96B+UJPSE/6ponZbsgkv7UWgErTwrhbvAtbo
KtdDAVG8YjcUpJr47T+cCe7mAEH8azHpfFBMPoqQrvVYFFCDMOJpS2vfGJYYWJj4
kO9UReIYo/BNl7nZ+U48pdTBVH09MLzufMEYw+Z+qjOEY/+HUnasIyh99C6DY3uX
srbWDJjURfS8FIcAEQEAAbQmU2ViYXN0aWFhbiB2YW4gV2FnZW5zdmVsZCA8bWVA
c2ViYi5hdT6JAc4EEwEIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQR1
PK626Y0rvj1gEjraeePDiD39cwUCY0/LxQAKCRDaeePDiD39c2iUC/4133MofoZ7
9Tlgtdcecpj7+JoVUTcsZe9AKtIm3NnmuZq/SWma9ZaQgfcZdk906AckUnmDxZwk
7qqwUAr8VxTNKTkVo135pWX2HJPc9IdGbFlpTMw5G0O9rAIxBemR1t+29iFWjTit
qM22lvhQnFpShRqYWaPAX2JisRNmJmM+mhkaMrBADIaDWKwfg7+V9ww6+HPDjhMA
BKb5B9C2ySTB77+r2MsDpzqwZwRrKKcCyssAcuQ8yscl5l38r2/aKymIh3raEGs4
8GLmSg397K9bMpFYx+1Al1h3109so0mJ577nCrF2XqHWeLhKxZ4u0O/W3CAaEpid
4qPAjNbGBnVxNV2uXGIjvK4g5a+coGWmvef7N/8MRTE/GGhaR/qAaSytRaahN898
dDCDE1lWZKRufD3YVP73m5r/MOEXomZ07CWubwQelRpSAD7Q0FGq5ltE74deUKzi
bVdC/kk9hwie5cks+POJs2vgxm2IqU9h/s6KBU/yNjjiayACoyEa81u0KFNlYmFz
dGlhYW4gdmFuIFdhZ2Vuc3ZlbGQgPG1lQHN2YW53LmNvbT6JAcQEEwEKAC4ECwkI
BwIVCgIWAQKeAQKbAxYhBHU8rrbpjSu+PWASOtp548OIPf1zBQJiQFOZAAoJENp5
48OIPf1zIsgL/iyjLjjYSmC+4K1eHNPAEtx+JVQE7jOHeGj/uN6igThN95Z5tJmK
WWyuP1oDgh44NEXp1DWynlSMoqQ6GKQiiBq/+us6kfHn2qIidMNPr2df/6TRzxU0
dqdt04y6aUDD53zRDSS/AbLxqXg9A0MGr+BU9nV3X+VWV0O+FWFBIjcHv0OXmp74
zsl937ZUvauqXU3gr/j/RTGx9X79aeSHRqYDWiLJSwnLBWFMbkv9dBRGCzbcUtq4
qQGQyRbLLNyvhGqFwIburnSKpFTDABYj6/86rShVl2YgqRp2PQj1Dcmv4TgAndYI
7/bRBp+E1YHKHYtlBKoK3nZ+jQ23bKcMcEk0QAvIXO2mv7DlCBVL6SkvsVfASi8A
uCm9GrH+8inV1d3vp6DmujyCN1PtorHBiwlzBgymq1wLIWSTXA3c6g+xFl2NvlRs
K7Or7bvjZXtPUtH/SyVXZwi0HkhmjakJxxXMnToclAgOqXGk1gzEWgGgS9/I3BB+
tCaf2TR7x14G6okBswQQAQgAHRYhBN8pJEAcSm/7szWITtEM2vw4gauVBQJg8q+n
AAoJENEM2vw4gauV/JwMAJvL1uKc24RIlLb7gACFCWip9w8UacTwIzvj1/qCkRPe
LHzunryd6q67mx96Q3JThOZcij7NSXn1m7cvaxxvlmOa6f5v3BTAXhBDMo0L6c/T
Mf2ym3SVs5ALcCsjj/5dUzhaijsxe0Ev/PYjml9Wz0zVinFGeQom2w6XplWQQVxs
gqwwn8phddiKEHC1OC9GXMhpSiPdpv8RPhwOVFIpJypzlPocZGfRD/aCOv7mMp3N
vV4BkZBEIIj6dZKPAxAYO3Gl3RN6gE++AA4SawLi9A+sJ+A0aD/YwtC7bj8Kwsm2
4378x0iq3sU+JPsuQk61dV6ww/rmDMkVFafHJZr9y1MrjUXb2+WW/NRrX6aqYdoj
x9GNXh/jf/dlRkB/Eg2zOaEVO5f94LCZbVkisU/q17SP3Arzek/ARki+9p0eMKzP
xomeaQzDUgCJV/4+FbSYB1yAdXC2BsXo3VRuh6kVuP309oumWasu1DXgB6RReeNT
pQBzPDJhncx8qLY39+NzibQVU2ViYXN0aWFhbiA8dEBzdncuYXU+iQHRBBMBCAA7
AhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAFiEEdTyutumNK749YBI62nnjw4g9
/XMFAmNPy8UCGQEACgkQ2nnjw4g9/XN+5Qv6AkF/keTyhVWuI2U+GlB9JuFU/fSv
QXr1BRbZPyYvQsmH2veb18TTr8BoM6hruC+MOTQXbL3WtoofXc2vrleGyo20c9xa
2i3oa87QOI/ig+5LnHsF3ypC+ldGpiT9C31Gf2wRSduyDNy/eJCD8G+D0Eb3DLv9
yiMReqplQ6h7wdxDYXtdLkbvuIgylTV+NgMzcR3sZAd2UGxocgXnjJ6H3z95lVOH
ikCkoQeTCtNmOCnYlwJs2UuHm3ZnPQZpnEIB3r3+zz4qf79o3nCw06acUA0aAauY
RJXrQYsWBGGcUq3xYz9l7ynjzdittqw0qoKYp+BnVssUDm7RqGWliFs4VNhOrdbK
us4ByeR/9aFguy6ulRDDzilZFvgXy0hy83AKQ1/N9qPStEkESDCU3vmDY/SL/Q7J
nRIHYW9rTkdC1PjVeKYqLVtv04sElo+JDYXXRpwinTJWG6Wv9MijorEdJQOVo7MY
ui8xzcXsK0sWEIWFEI15lCV/7XMhBAp899CAuQGNBGCR738BDADE3A+/ZaQn4ZjK
mej1+/Qgp3mGxwWKl/PnQ++iumuVVvh+KBIWaZvVnwNt0KytEAj+Jb7vvT14aa6L
i7GsmrxBrN97MshZQp4PmvvC8AbwMpvIIP+icbiEOrtHglpPiD690OkDNKQ/4RMd
EOerHTpJD+CkKld+rFiGDAJF4dxj5pALezatWo/MrkF/RbtTrShF7QG1zffJpM1T
T05jhc3GU5Rspl/15AFP7uFc78Tasqd2PSkPd34LPTBdnJwvOvbpXxtSJI6SvmVJ
S8SKoLF4NIKMjZ7VqrAvITchbl6pqXTrJWH9tzAx1ir0Rt25eDmFC+PJt/qRTvnR
O2qGtlXNzT7pXjUVWsryP6Uv83zOlzuU7+Bw1uflJ6ENJxTgVbkw0prVkrfl8QsY
1ZQtI8b45ikjlwqLfd0AiO01mnFxjT1HpO+iSk7JquMs2OHa2JPNYrd2ifuaADrQ
lnERbKU4K++0+/cxaSVFO6YBfaXilHF3+wHfhRRANnuBjUMnegUAEQEAAYkBtgQY
AQoACQWCYJHvfwKbDAAhCRDaeePDiD39cxYhBHU8rrbpjSu+PWASOtp548OIPf1z
kMEL/101aW27owNZj3QPSIR8NLvUBMIGiL2E6gwVpyVwcyodmL25K6xA6Jxj3662
8vJ2Ie0bE7xbKelLqotHVCZWuwqnQK8Z4teRA+6SnODbjjdzh9kLTjWiHTPpI/Vp
beH/zPivT12ZfyL/Djf56Z9gcAw4Noj0yLku4otbTS+56GU+699AwDiIsTypa750
Vkn54vRdSUaQsayJ6rbFhUMOujJNNycLk+RDnquSpzKtPY3iRAsYznGCjwxgbF8U
uVT5UXW627ywnz52nYL4V3WAu9dbkQUIkGCb9ZizDljbW4nV0YV4OM8dBo1zM8/D
VBsCBrKX0JWLPvTD/NJ2Dh7u2nLYSo7B71qfmggqRgjsUU3BfDSX9fuzc0nCEWhr
HalefIq6Vl/fIIAoeL33VTe6luI67vjlKz0Qz8n7DkPh1jScGnvvxlCifCVyJ+0u
MrmJkQDP4dSzVziJRq3eojSAEI/A3/zftlhun5Ri2QTSiYxbTBltLKmjDhjkHt+E
ERUWbw==
=Dq/C
-----END PGP PUBLIC KEY BLOCK-----
```

</details>

I'm also on the following social networks (stretching the definition of both "on" and "social networks"):

- [Gitlab](https://gitlab.com/wagensveld)
- [Github](https://github.com/wagensveld)
- [LinkedIn](https://www.linkedin.com/in/wagensveld)
