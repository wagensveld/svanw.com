---
title: "スタティックサイトでPGP Web Key Directoryをホストする"
description: "自動キー検出のために静的WebsiteでWKDをホストする方法に関するクイックガイド。"
summary: "自動キー検出のために静的WebsiteでWKDをホストする方法に関するクイックガイド。

私の電子メールで試してみることができます: `gpg --locate-keys t@svw.au`"
date: "2021-07-27T21:11:59"
category: "guides"
---

> GPG が既にインストールされており、PGP キーが設定されていることを前提としています。

メールアドレスが`name@domain.com`であるとしましょう、以下を実行してください：

```
gpg --with-wkd-hash --fingerprint name@domain.com
```

出力は次のようになります:

```
pub   rsa3072 2021-07-27 [SC]
      4481 A072 E549 8EC2 A6D2  EB39 2C07 68F3 1CCD 1215
uid           [ultimate] John Citizen <name@domain.com>
              pmw31ijkbwshwfgsfaihtp5r4p55dzmc@domain.com
sub   rsa3072 2021-07-27 [E]
```

ドメインの前にランダムに見えるハッシュが表示されます。この場合は`pmw31ijkbwshwfgsfaihtp5r4p55dzmc`です。これは WKD ハッシュです。このサイトに必要です。

ファイルにエクスポートします:

```
gpg --no-armor --export name@domain.com > pmw31ijkbwshwfgsfaihtp5r4p55dzmc
```

電子メールとハッシュを独自の値に置き換えます。

次に、この生成されたファイルをディレクトリ`/.well-known/openpgpkey/hu/`の下の Web サイトにアップロードする必要があります。これは SSH 経由で実行できますが、サーバーは管理されています。 私の場合、hugo を使用しているため、ファイルは最終的に`/static/.well-known/openpgpkey/hu/`になります。

これで、キーは`gpg --locate-keys name@domain.com`を介して検出できるはずです。
