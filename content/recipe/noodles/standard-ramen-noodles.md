---
title: "Standard Ramen Noodles"
description: "Fairly versatile alkaline noodle recipe."
summary: "Fairly versatile alkaline noodle recipe."
date: "2022-09-29T10:29:09"
# images:
#  - "images/standard-ramen-noodles.webp"
series:
  - "ramen"
category: "recipes"
---

> Makes 4 servings

> Make [kansui powder](/recipe/misc/kansui-powder) beforehand.

1. Add kansui powder to water, then add salt. Dissolve completely. (You can do this days in advance.)
   - **Water**, 152ml
   - **Kansui Powder**, 4.8g
   - **Salt**, 4g
2. In a stand mixer with a paddle attachment combine flour, bread improver and egg replacer.
   - **Bread Flour**, 396g
   - **Bread Improver**, 4g
   - **Egg Replacer**, 4g
3. Slowly add the kansui solution to the dry ingredients over 3 or so minutes until small clumps begin to form. (Should be very small clumps.)
4. Cover mixture with cling wrap, seal and let rest for 1 hour at room temperature or 8 hours in the fridge.
5. By hand grab and clump together some dough. Use a pasta maker to knead the dough, go from the largest to the 2nd then 3rd setting. Then take the dough out fold it in half, and repeat until the sheet is smooth.
6. The dough back into resealable bag and let rest at room temperature for another 30 minutes.
7. Cut dough into workable portions, then roll out desired thickness - using corn starch to prevent sticking.
8. Cut noodles into desired thickness.
9. Bundle noodles into 130g portions, put in a resealable bag and let rest in the fridge for at least a day.
10. Boil for about a minute in boiling water, strain.
