---
title: "Kansui Powder"
description: "Kansui is an alkaline solution. It has a variety of uses, one of them being to make alkaline noodles such as ramen. For storage's sake I like to make it in powdered form."
summary: "Kansui is an alkaline solution. It has a variety of uses, one of them being to make alkaline noodles such as ramen. For storage's sake I like to make it in powdered form."
date: "2022-09-23T19:21:05"
series:
  - "ramen"
category: "recipes"
---

## Authentic Kansui

If possible: by weight, combine sodium carbonate (40%) with potassium carbonate (60%).

## At Home

Because sodium carbonate and potassium carbonate aren't readily available in stores, heres a recipe to turn baking soda into sodium carbonate. It should work for potassium bicarbonate but finding potassium carbonate is just as easy here.

In recipes it's generally okay to use pure sodium bicarbonate instead of the mixture, but it isn't preferred.

> Wear gloves, and use good ventilation

1. Add baking soda to a dry pan on medium high heat.
   - **Baking soda**, 300g
2. Stir occasionally, you'll see pockets of air escape, when it stops (about 20 minutes). Kill the heat and let rest until cool.
3. The resulting powder should weigh about 2/3 the original weight (200g).
