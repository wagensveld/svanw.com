---
title: "Set up spellcheck for markdown files"
description: "Using CSpell"
summary: "Using CSpell"
date: "2023-09-28T21:13:24"
category: "blog"
---

This website is made up of markdown files. The problem is my spelling is awful. But fortunately there's [CSpell](https://cspell.org/).

There are a few ways to set it up, all have some place in my workflow.

## VSCodium

[Extension](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

And in my case the [Australian English extension](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-australian-english).

## Locally

```
npm install -g cspell
```

Running `npx cspell .` gave me `CSpell: Files checked: 353, Issues found: 20707 in 326 files`. So there's probably a good deal of words to clean up, as well as configuration to be done.

Such as installing the Australian language pack.

```
npm install -g @cspell/dict-en-au
npx cspell link add @cspell/dict-en-au
```

`CSpell: Files checked: 354, Issues found: 1237 in 160 files` is a lot more manageable.

## Gitlab CI

Street side Software offer a [docker image](https://github.com/streetsidesoftware/cspell-cli/pkgs/container/cspell) which doesn't include `en-AU`.

Creating a stage in Gitlab CI is straightforward enough:

```yaml
spell_check:
  stage: build
  image:
    name: node:latest
  script:
    - npm install
    - npx cspell link add @cspell/dict-en-au
    - npx cspell --no-progress .
```

## Pre-commit

Having spell check in CI is great but ideally I wouldn't even commit typos. Lucky enough [pre-commit](https://pre-commit.com/) is supported!

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://github.com/streetsidesoftware/cspell-cli
    rev: v7.3.0
    hooks:
      - id: cspell
```

This will only run on modified files. In 99% of cases that's 100% of the typos. The CI is still useful for those times where I'm stuck troubleshooting `.gitlab-ci.yml`.

## Config

If you followed this far you probably have a lot of errors and failed builds right now. (The amount of `git commit --no-verify` I've done is shocking.)

My `cspell.config.yaml` is fairly basic right now but it'll be refined as time goes on.

```yaml
---
$schema: https://raw.githubusercontent.com/streetsidesoftware/cspell/main/cspell.schema.json
version: "0.2"
language: "en-AU"
ignorePaths:
  - content/rough-drafts
dictionaryDefinitions:
  - name: aws
    path: "./cspell-dictionaries/aws.txt"
    addWords: true
  - name: other
    path: "./cspell-dictionaries/other.txt"
    addWords: true
  - name: names
    path: "./cspell-dictionaries/names.txt"
    addWords: true
dictionaries:
  - aws
  - other
  - names
words: []
ignoreWords: []
import: []
```
