---
title: "Home Film Developing Tables"
description: "Some simple tables for the film development process at home."
summary: "Some simple tables for the film development process at home."
date: "2024-07-01T20:24:26"
category: "misc"
series:
  - "photography"
---

## Mixing Chemicals

### Developer

#### Fomadon P D-76

1. Warm 700ml water to 40°C.
2. Dissolve contents of small bag.
3. Dissolve contents of large bag.
4. Mix in 300ml water (Total 1L).

#### Fujifilm SPD ({{< furigana "スーパー" "Super" >}} {{< furigana "プロドール" "Prodol" >}})

1. Warm 1L water to 40°C.
2. Dissolve contents of bag.

### Fixer

#### Fomafix P (Film)

1. Warm 600ml water to 40°C.
2. Dissolve contents of large bag.
3. Dissolve contents of small bag.
4. Mix in 150ml water (Total 750ml).

#### Fomafix P (Paper)

1. Warm 600ml water to 40°C.
2. Dissolve contents of large bag.
3. Dissolve contents of small bag.
4. Mix in 400ml water (Total 1L).

## Developing

### D-76 / ID-11
> 1L: 10 uses, 20°C, increase time by 15% after 4th roll

Continuously agitate for the first **_30 seconds_**. Then for the first **_10 seconds_** every minute.

| Film                          | Minutes  |
| ----------------------------- | -------- |
| FilmNeverDie Kuro 100         | 10       |
| Fomapan 100 Classic           | 7 ~ 8    |
| Fomapan 200 Creative          | 5 ~ 6    |
| Fomapan 400 Action            | 10 ~ 12  |
| Ilford Delta 400 Professional | 9 ~ 10   |
| Ilford HP5 Plus 400           | 7 ~ 8    |
| Ilford Ortho Plus 80          | 8        |
| Ilford SFX 200                | 10       |
| Kentmere 400                  | 9 ~ 10   |
| Kodak TMax 100                | 6 ~ 7    |
| Kodak TMax 400                | 7 ~ 8    |
| Kodak RAR Film 2495           | 7        |

### Fujifilm SPD ({{< furigana "スーパー" "Super" >}} {{< furigana "プロドール" "Prodol" >}})
> 1L: 10 uses, 20°C, increase time by 5-10% after 4th roll

Continuously agitate for the first **_minute_**. Then for the first **_5 seconds_** every minute.

| Film                  | Minutes  |
| --------------------- | -------- |
| FilmNeverDie Kuro 100 | 10       |
| Fomapan 400 Action    | 10 ~ 12  |
| Ilford HP5 Plus 400   | 7 ~ 8    |


### Rodinal

There are two ways in which I use Rodinal.

1. As a normal developer
2. For stand developing

I don't usually do stand developing, but I find it especially useful for mystery film. Occasionally at markets I'll find either very expired film, or something that was bulk rolled from an unknown origin. I find stand developing gives me a nice workable image. I don't think I would prefer it over using a regular developer in other cases.

#### Normal Developing (1+50)
> ~1L (20ml Rodinal to 1L water, or for single steel developing tank 6ml Rodinal to 300ml water), 20°C, discard after use.

Continuously agitate for the first **_30 seconds_**. Then for the first **_10 seconds_** every minute.


| Film                  | Minutes  |
| --------------------- | -------- |
| Ilford HP5 Plus 400   | 11       |

#### Stand Developing (1+100)
> ~1L (10ml Rodinal to 1L water, or for single steel developing tank 3ml Rodinal to 300ml water), 20°C, discard after use.

Continuously agitate for the first **_30 seconds_**. Then optionally once or twice about half way through the developing time.


| Film | Minutes  |
| ---- | -------- |
| Any  | 60       |

## Fixing

### Fomafix P
> 750mL: 15 uses, 20°C

Continuously agitate for the first **_30 seconds_**. Then for the first **_10 seconds_** every minute.

| Medium | Minutes |
| -------|-------- |
| Any    | 10      |
