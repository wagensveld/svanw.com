---
title: "Host a PGP Web Key Directory on a Static Site"
description: "A quick guide on how to host a WKD on a static website for automatic key discovery."
summary: "A quick guide on how to host a WKD on a static website for automatic key discovery. You can give it a shot with my email `gpg --locate-keys t@svw.au`"
date: "2021-07-27T21:11:59"
category: "guides"
---

> This assumes you already have GPG installed and a PGP key set up

## Generate and export WKD hash

Lets say your email is `name@domain.com`, run the following:

```
gpg --with-wkd-hash --fingerprint name@domain.com
```

You'll end up with an output which looks like this:

```
pub   rsa3072 2021-07-27 [SC]
      4481 A072 E549 8EC2 A6D2  EB39 2C07 68F3 1CCD 1215
uid           [ultimate] John Citizen <name@domain.com>
              pmw31ijkbwshwfgsfaihtp5r4p55dzmc@domain.com
sub   rsa3072 2021-07-27 [E]
```

You'll see a random looking hash before your domain, in this case `pmw31ijkbwshwfgsfaihtp5r4p55dzmc`, this is the WKD hash, we need this for our site.

Export it to a file:

```
gpg --no-armor --export name@domain.com > pmw31ijkbwshwfgsfaihtp5r4p55dzmc
```

Replacing the email and hash with your own values.

## Upload the file

Now you need to upload this generated file to your website under the directory `/.well-known/openpgpkey/hu/`, this can be done via SSH or however your server is managed. In my case because I use Hugo, my file ends up going in `/static/.well-known/openpgpkey/hu/`.

You also want to create an empty policy file: `/.well-known/openpgpkey/policy`.

## Verify key discovery

Your key should now be discoverable via `gpg --locate-keys name@domain.com`.
