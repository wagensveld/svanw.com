---
title: "Copy an ISO to a USB for an installer on macOS"
description: "dd if=image.iso of=/dev/ridentifier bs=1m"
summary: "```
dd if=image.iso of=/dev/ridentifier bs=1m
```"
date: "2022-02-28T21:18:16"
category: "guides"
---

1. Identify your disk `diskutil list`

```
diskutil list

...
/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *15.5 GB    disk2
   1:                  FAT_32 Untitled                   15.5 GB    disk2s1
```

Make note of the identifier of the volume. In this case `didisk2s1`

2. Unmount the volume `sudo umount /dev/<IDENTIFIER>` (`sudo umount /dev/didisk2s1`).
3. Convert and copy file `sudo dd if=</path/to/image.iso> of=/dev/r<IDENTIFIER> bs=1m` (`sudo dd⋅if=image.iso⋅of=/dev/rdisk2s1⋅bs=1m"`)
4. Eject the volume `diskutil eject /dev/<IDENTIFIER>` (`diskutil eject /dev/disk2s1`)
