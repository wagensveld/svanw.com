---
title: "Adana kebab"
description: "My girlfriend and I had Adana at a restaurant and liked it so much we made it for Valentines day."
summary: "My girlfriend and I had Adana at a restaurant and liked it so much we made it for Valentines day."
date: "2022-02-20T23:19:11"
category: "recipes"
---

1. Dice onion very finely, add to a bowl.
   - **Onion**, 1 -- Small
2. Mice garlic, add to the bowl.
   - **Garlic**, 5 Cloves
3. Add all other ingredients to bowl, knead. Let rest for 30 minutes.
   - **Lamb**, 500g -- Minced
   - **Cumin**, 1 tsp
   - **Sumac**, 1 tsp
   - **Salt**, 1/2 tsp
   - **Pepper**, 1/4 tsp
   - **Chilli Flakes**, 1/4 tsp
   - **Water**, 2 Tbsp
4. Shape into egg shaped patties.
5. Grill for about 12 minutes, sprinkle extra cumin and sumac as it's grilling.
