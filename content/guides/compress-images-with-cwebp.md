---
title: "Compress Images With cwebp"
description: "Simple way to compress images for web usage. So simple in fact the only reason I have this post is because I forget about the tool every now and then."
summary: "Simple way to compress images for web usage. So simple in fact the only reason I have this post is because I forget about the tool every now and then."
date: "2022-11-30T19:51:32"
category: "guides"
---

1. Install cwebp: `npm install -g cwebp`
2. `cwebp -q 75 IMAGE.png -o IMAGE.webp`

The `-q` determines quality, in fact in this example you don't need to include it because cwebp defaults to 75.
