---
title: "NetworkManager Tips (Now with a WiFi 6e fix)"
description: "Of all of these archbtw posts, this is the one you can skip. I don't see much tangible benefit. Edit: It turns out I missed something pretty big before preventing me from using WiFi 6e, this has been updated with that fix."
summary: "Of all of these archbtw posts, this is the one you can skip. I don't see much tangible benefit. Edit: It turns out I missed something pretty big before preventing me from using WiFi 6e, this has been updated with that fix."
date: "2023-12-27T13:20:53"
series: ["linux"]
category: "guides"
---

## Unique DUID Per Connection

Create a file `/etc/NetworkManager/conf.d/duid.conf` with the content:

```
[connection]
ipv6.dhcp-duid=stable-uuid
```

## IPv6 Privacy Extensions

Create `/etc/NetworkManager/conf.d/ip6-privacy.conf` with the contents:

```
[connection]
ipv6.ip6-privacy=2
```

## Use iwd as the WiFi Backend

Install `iwd`

```
pacman -S iwd
```

Create the file `/etc/NetworkManager/conf.d/wifi_backend.conf` with the contents:

```
[device]
wifi.backend=iwd
```

[You may have to convert existing NetworkManager profiles.](https://iwd.wiki.kernel.org/networkmanager#converting_network_profiles)

```
cd /etc/NetworkManager/system-connections
mkdir ../system-connections-iwd
for f in *; do grep -v '^\(mac-address\|interface-name\|permissions\|bssid\)=' "$f" > ../system-connections-iwd/"$f"; done
chmod 0600 ../system-connections-iwd/*
cd /etc/NetworkManager
mv system-connections system-connections-backup
dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager/Settings org.freedesktop.NetworkManager.Settings.ReloadConnections
sleep 1
mv system-connections-iwd system-connections
dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager/Settings org.freedesktop.NetworkManager.Settings.ReloadConnections
```


## Sharing  Internet Connection

`dnsmasq` allows you to share your Internet connection with other devices.

Install `dnsmasq`

```
pacman -S dnsmasq
```

Then set NetworkManager to use it, create this file: `/etc/NetworkManager/conf.d/dns.conf` with the content:

```
[main]
dns=dnsmasq
```

Afterwards it's just a matter of creating a new shared connection.

## WiFi 6e Fix

By the time I wrote this post I had gone through a few installations of Arch, so I'm not entirely sure how I missed this before.

WiFi 6e wasn't working, it turns out I didn't have a regulatory domain set up.

```
~ ❯ iw reg get
global
country 00: DFS-UNSET
	(2402 - 2472 @ 40), (6, 20), (N/A)
	(2457 - 2482 @ 20), (6, 20), (N/A), AUTO-BW, PASSIVE-SCAN
	(2474 - 2494 @ 20), (6, 20), (N/A), NO-OFDM, PASSIVE-SCAN
	(5170 - 5250 @ 80), (6, 20), (N/A), AUTO-BW, PASSIVE-SCAN
	(5250 - 5330 @ 80), (6, 20), (0 ms), DFS, AUTO-BW, PASSIVE-SCAN
	(5490 - 5730 @ 160), (6, 20), (0 ms), DFS, PASSIVE-SCAN
	(5735 - 5835 @ 80), (6, 20), (N/A), PASSIVE-SCAN
	(57240 - 63720 @ 2160), (N/A, 0), (N/A)
```

Installing `wireless-regdb` fixed the problem.

If not you may need to run `iw reg set AU` to set it to Australia.

```
sudo pacman -S wireless-regdb
```
