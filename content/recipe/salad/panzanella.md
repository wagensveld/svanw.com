---
title: "Panzanella"
description: "One of my favourite salads."
summary: "One of my favourite salads."
date: "2023-01-01T19:12:34"
category: "recipes"
---

> Preheat oven to **180°C_**.

1. Cut sourdough into cubes (about 2cm x 2cm). Add to a bowl with a small amount of olive oil. Toss around.
   - **Sourdough**, 80g
   - **Olive Oil**, To Taste
2. Add sourdough to an oven at 180c for 15 minutes. Use this time to prepare the tomatoes.
3. Cut tomatoes into bite sized pieces.
   - **Tomatoes**, 250g
4. Add tomatoes to colander over the bowel, add salt. Let rest for at least 15 minutes, meanwhile start cutting other ingredients.
   - **Salt**, 2g
5. Cut shallots finely, add to bowl with extracted tomato liquid.
   - **Shallot**, 1 -- Small
6. Add garlic, mustard, vinegar, olive oil, salt, and pepper to bowl; and mix. Adjust ingredients to taste.
   - **Garlic**, 1 Clove -- Minced
   - **Dijon Mustard**, 1/2 tsp
   - **White Wine Vinegar**, 10ml
   - **Olive Oil**, To Taste
   - **Salt**, To Taste
   - **Pepper**, To Taste
7. Roughly chop basil, add it, tomatoes, and bread to bowl.
   - **Basil**, To Taste
   - **Tomatoes**
   - **Bread**
8. Let rest for at least 30 minutes.
