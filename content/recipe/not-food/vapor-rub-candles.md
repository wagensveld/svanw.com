---
title: "Vapour Rub Candles"
description: "Originally tried to buy these on Etsy, but then they cancelled my order, so here's the DIY version."
summary: "Originally tried to buy these on Etsy, but then they cancelled my order, so here's the DIY version."
date: "2023-02-21T21:33:43"
category: "recipes"
---

1. Heat the wax flakes. You can use low heat on a stove, or short 10 second bursts in the microwave. Until broken down and able to be mixed.
   - **Soy wax flakes**, 240g
2. Add essential oils to wax. mix gently.
   - **Eucalyptus essential oil**, 10 drops
   - **Peppermint essential oil**, 10 drops
   - **Tea tree essential oil**, 10 drops
   - **Lavender essential oil**, 10 drops
3. Add wick to jar, pour in wax.
   - **Blue jar**, 1 -- 200ml, Blue
   - **Wick**, 1 -- 10cm
