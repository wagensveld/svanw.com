---
title: "Pollo Asado"
description: "A pretty inauthentic pollo asado."
summary: "A pretty inauthentic pollo asado."
date: "2021-10-24T20:23:46"
category: "recipes"
---

1. Add Chilli Powder, Oregano, Mayonnaise, Salt, and Pepper to a bowl.
   - **Chilli Powder**, 1/2 tsp
   - **Oregano**, 2 tsp
   - **Mayonnaise**, 1 Tbsp
   - **Pinch of Salt**
   - **Pinch of Pepper**
2. Crush Garlic, add to bowl.
   - **Garlic**, 2 Cloves -- Crushed
3. Zest and then juice Lime, add to bowl and mix.
   - **Lime**, 1/2
4. Add Chicken to bowl and ensure even coating.
   - **Chicken Thighs**, 250g
5. Cook in pan until brown on each side.
