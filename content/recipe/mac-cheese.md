---
title: "Coma Mac and Cheese"
description: "I made this with my girlfriend for our first Christmas Dinner and it puts you into a food coma."
summary: "I made this with my girlfriend for our first Christmas Dinner and it puts you into a food coma."
date: "2021-12-20T20:36:28"
category: "recipes"
---

1. Cook macaroni until slightly undercooked according to pasta instructions, strain.
   - **Macaroni**, 600g
2. Mix butter in cooked macaroni.
   - **Butter**, 25g
3. Grate cheeses, set aside.
   - **Medium Cheddar**, 340g
   - **Light Cheddar**, 230g
   - **Sharp Cheddar**, 110g
   - **Mozzarella**, 170g
   - **Monterey Jack**, 130g
   - **Colby**, 100g
4. In a pot on low heat mix butter, flour, and garlic (crushed) until golden.
   - **Butter**, 70g
   - **Flour**, 20g
   - **Garlic**, 5 Cloves
5. Whisk in milk and cream until thickened. Meanwhile preheat oven to 250C.
   - **Milk**, 800ml
   - **Cream**, 220ml
6. Remove heat and whisk in cheeses.
   - **Cheese**
7. Whisk in garlic powder, onion powder, paprika, and black pepper.
   - **Garlic powder**, 1/2 tsp
   - **Onion powder**, 1/4 tsp
   - **Paprika**, 3/4 tsp
   - **Black pepper**, 1/4 tsp
8. In a tray mix pasta with cheese, sprinkle Italian herbs on top. Broil in oven for 10 minutes.
   - **Italian Herbs**
