---
title: "Espresso Drinks"
description: "There are many many resources online about the various types of espresso based drinks. At times I've found it a bit overwhelming. Hopefully these definitions and recipes can act as a good starting point for anyone getting started in the world of espresso drinks."
summary: "There are many many resources online about the various types of espresso based drinks. At times I've found it a bit overwhelming. Hopefully these definitions and recipes can act as a good starting point for anyone getting started in the world of espresso drinks."
date: "2022-09-21T22:25:14"
aliases:
  - "/espresso"
toc: true
category: "food"
---

Often recipes are broken into ratios. For example 1:2 ratio for an espresso shot means: for every 1 gram of coffee bean which goes in, 2 grams of espresso comes out.

Why use mass as a measurement instead of volume?

While pulling a shot of espresso CO<sub>2</sub> is released forming bubbles known as _crema_. The amount of crema developed and how quickly they dissipate varies, mass is a more consistent way to measure it.

In my opinion weighing up milk isn't as important, the amount of milk to add is personal preference. As a guide I'll use ml prior to steaming, (at the same time 100ml of milk is roughly 105g, which seems close enough if you prefer weighing).

## Shots

At the core of an espresso drink is the espresso shot itself, there are three main types of espresso shots:

- Ristretto
- Normale (Espresso)
- Lungo

Traditionally a shot of espresso is about 7g. These days most recipes call for double shots, closer to 14g. A lot of after market baskets are designed for double shots. The size of the shot doesn't change the extraction time you should aim for. E.g. if you have 7g in you want 14g out in 30 seconds, if you have 16g in you want 32g out in 30 seconds. Personally I use a 16g basket, so it's what I'll be using for this post.

### Ristretto

> 1:1 in 20-30 seconds

The ristretto is the most concentrated of the shots. It follows a 1:1 ratio so for 7g of bean you're aiming for 7g of coffee in about 25 to 30 seconds. Compared to a normale you want to grind finer.

This results in quite a bold coffee albeit a little sour. While you can definitely drink it on its own, often it's used as the base of a milk drink.

### Normale (Espresso)

> 1:2 in ~30 seconds

Normale is the standard espresso, so much so often when someone says espresso they mean normale. Its ratio is 1:2, so 7g in 14g out in about 30 seconds.

It produces a balanced shot also often used in milk drinks, but enjoyed on its own as well.

Often it is served as a _doppio_ which means double. Instead of 7g in 14g out, you get 14g in 28g out.

Normale is also known as a _short black_ in Australia.

### Lungo

> 1:3 in 30-35 seconds

Lungo is in some ways the opposite of a ristretto. Its ratio is 1:3, 7g in 21g out in 30 to 35 seconds. Compared to a normale you want to grind coarser.

The shot is fairly bitter, but at the same time it has a high water content making it better to drink on its own.

### Side Tangent: Pressure and Temperature

> You can skip this section, it's getting into the weeds of it.

At the moment the standard pressure is 9 Bar, most machines will come stock with 9 Bar. These shots are fairly standard with crema. Other options are 12 Bar which comes with the Gaggia Classic - this generally produces stronger bitterness, more pronounced acidity and more crema. And 6 Bar which is included in the 9 Bar replacement kit for the Gaggia Classic - generally 6 Bar won't produce crema, but are quite sweet and not very bitter.

As for temperature [five senses has a very good post on the matter](https://fivesenses.com.au/blogs/news/brew-temperature-and-its-effects-on-espresso), the key takeaways are high temperatures result in increased extraction, sweetness, bitterness, and body.

## Milk Recipes

For the recipes I have a couple of assumptions:

- The ristretto/normale shots are done in a double basket, creating 16g or 32g shots respectively. Most recipes call for a double shot.
- I use all the milk - often it makes sense to use more milk to help with steaming.

### Ristretto Drinks

#### Magic

> 2 shots ristretto with 130ml steamed milk

The magic is an Australian coffee very specific to the Melbourne's CBD. So much so that neighbouring suburbs often don't serve it. The vast majority of places don't list it as a menu item, so if you want to feel like a local magic is your coffee.

A magic is made up of the following:

- 2 shots ristretto (16g in 16g out total)
- 130ml steamed milk (little to no foam, heated to approximately 60C)

Served in a 160ml cup.

Compared to more common milk drinks such as a latte the magic is a stronger drink. The closest common drink would be a latte with an extra shot.

#### Piccolo

> 1 shot ristretto with 75ml steamed milk

The piccolo is another Australian coffee, this time from Sydney, and as now served all across the world.

To make it you need to do the following:

- 1 shot ristretto (8g in 8g out total)
- 75ml steamed milk (little to no foam, heated to approximately 60C)

Served in a 90ml glass.

In a lot of ways it's like a latte but smaller, very similar profile.

#### Cortado

> 2 shots ristretto with 60ml steamed milk

Cortados are from Spain, and are fairly similar to piccolos but stronger. The traditional recipe is a 1:1 ratio of espresso to milk.

Recipe:

- 2 shots ristretto (16g in 16g out total)
- 60ml steamed milk (little to no foam, heated to approximately 60C)

### Normale Drinks

#### Flat White

> 2 shots normale with 180ml steamed milk

The origins of the flat white are contested, it's either Sydney, Australia, or Wellington, New Zealand. It's called a flat white because the milk only get steamed enough to make microfoam, not froth like the lattes and cappuccinos of the time.

To make one:

- 2 shots normale (16g in 32g out total)
- 180ml steamed milk (little to no foam, heated to approximately 60C)

#### Latte

> 2 shots normale with 180ml steamed milk

The latte is also contentious, in Australia at least lots of cafes serve lattes and flat whites as the same drink. Some places do make a distinction though: Lattes have a slight amount of foam on top, not much, just a really small amount.

Recipe:

- 1 shot normale (16g in 32g out total)
- 180ml steamed milk (small amount of foam, heated to approximately 60C)

Served in a glass.

#### Cappuccino

> 2 shots normale then 1/8 tsp cocoa powder with 180ml steamed milk with a cocoa powder dusting

Cappuccinos have also fallen into contention two recipes come to mind:

Traditional:

- 2 shots normale (16g in 32g out total)
- 180ml steamed milk (large amount of foam - half foam, half milk , heated to approximately 60C)

Serve with milk and foam as distinct layers.

Australian:

- 2 shots normale (16g in 32g out total)
- 1/8 tsp cocoa powder
- 180ml steamed milk (little to no foam, heated to approximately 60C)
- Dusting of cocoa powder on top

Add 1/8 tsp cocoa powder first so espresso dissolves it.

#### Mocha

> 2 tsp cocoa powder then 2 shots normale with 180ml steamed milk with a cocoa powder dusting

On the topic of adding chocolate to milk drinks the mocha.

Recipe:

- 2 shots normale (16g in 32g out total)
- 2 tsp cocoa powder
- 180ml steamed milk (little to no foam, heated to approximately 60C)
- Dusting of cocoa powder on top

Add 2 tsp cocoa powder first so espresso dissolves it.

#### Macchiato

> 1 shot normale with a dollop of foam

Macchiato is an espresso with a dollop of foam on top, creating a solo drink is tough given how little foam you actually need.

In Australia it's often called a _short mac_ for a single (16g) shot, or a _long mac_ for a double (32g) shot.

Recipe:

- 1 shot normale (8g in 16g out total)
- 15ml steamed milk (completely foamed, heated to approximately 60C)

## Water Recipes

### Long Black

> 120ml hot water with 1 shot normale

Another Australian drink, the long black is espresso added to hot water, this way the crema is undisturbed keeping its texture.

Recipe:

- 120ml hot water (approximately 90C)
- 1 shot normale

### Americano

> 1 shot normale with 120ml hot water

The americano is believed to originate from Italy during WW2, when American G.I.s diluted espresso to get it to more closely resemble percolator coffee which was popular in America at the time. It's the reverse of a long black, hot water added to espresso.

Recipe:

- 1 shot normale
- 120ml hot water (approximately 90C)

## Cold Recipes

Iced variants can be made of any of the above drinks by adding ice to the drink and not steaming the milk.

### Affogato

> 2 scoops ice cream with 2 shots normale

A delightful summertime dessert. Affogato's full name is "Affogato al caffe" meaning "Drowned in coffee", which is exactly whats happening to the ice cream.

Recipe:

- 2 scoops ice cream
- 1 shot normale (16g in 32g out total)
