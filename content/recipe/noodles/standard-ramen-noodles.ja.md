---
title: "定番ラーメン"
description: "かなり用途の広いアルカリ麺レシピ。"
date: "2022-09-29T10:29:09"
series:
  - "ramen"
category: "recipes"
---

かなり用途の広いアルカリ麺レシピ。

<!--more-->

> 4 人分

## 材料

| 材料                                       | 量    |
| :----------------------------------------- | :---- |
| 水                                         | 152ml |
| [枧水粉](/ja/recipe/noodles/kansui-powder) | 4.8g  |
| 塩                                         | 4g    |
| パン粉                                     | 396g  |
| パン用改良剤                               | 4g    |
| 卵代替品                                   | 4g    |

## 作り方

1. 水に枧水粉を入れ、塩を加えます。 完全に溶解します。(これは数日前に行うことができます。)
   - 水 152ml
   - 枧水粉 4.8g
   - 塩 4g
2. パドルアタッチメントを備えたスタンドミキサーで、粉、パン用改良剤 、および卵代替品を混ぜ合わせます.
   - パン粉 396g
   - パン用改良剤 4g
   - 卵代替品 4g
3. 小さな塊が形成され始めるまで、湿った成分と乾燥した成分を 3 分ほどかけて混ぜ合わせます。(非常に小さな塊である必要があります。)
4. 混合物をラップで覆い、密閉して、室温で 1 時間または冷蔵庫で 8 時間休ませます。
5. 手で生地をつかみ、ひとまとまりにします。 パスタメーカーを使って生地をこね、一番大きなものから 2 番目、3 番目の設定にします。 生地を取り出し、半分に折り、シートが滑らかになるまで繰り返します。
6. ジップロックに戻し、室温で 30 分休ませる。
7. 生地を使いやすい大きさにカットし、くっつかないようにコーンスターチを使って好みの厚さに伸ばす。
8. 麺を好みの太さに切る。
9. 麺を 130g ずつにまとめ、ジップロックに入れて冷蔵庫で 1 日以上休ませる。
10. 沸騰したお湯で 1 分ほど茹で、漉す。
