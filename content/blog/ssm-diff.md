---
title: "SSM Diff"
description: "A fork of a fork of a fork of an SSM Diff tool written in python."
summary: "A fork of a fork of a fork of an SSM Diff tool written in python."
date: "2021-12-29T21:28:56"
images:
  -  "images/ssm.webp"
series: ["linux", "python", "aws"]
category: "projects"
---

[Code can be found here.](https://gitlab.com/wagensveld/ssm-diff)

SSM Diff is a tool written by [runtheops on GitHub](https://github.com/runtheops). Multiple forks of this code already exist, each addressing a change in the technology it depends on. Mine happened to be incompatibility with Python 3.9.

The tool is fairly simple to use:

1. Have the AWS CLI set up on your terminal. (If you're using an environment variable based authentication process make sure `AWS_DEFAULT_REGION` is set.)
2. In the ssm-diff directory, make sure the dependencies are installed: `pip install -r requirements.txt`.
3. Run `ssm-diff init` to initialise the default `parameters.yml`.
4. Run `ssm-diff pull` to clone the parameters from AWS into your parameters.yml file.
5. Run `ssm-diff plan` to see changes made to your SSM parameters.
6. Run `ssm-diff push` to push changes made to your SSM parameters.

Values for `/Dev/DBServer/MySQL/app1`, `/Dev/DBServer/MySQL/app2`, `/Dev/EC2/Web/app1`, `/Test/DBServer/MySQL/app1`, `/Test/DBServer/MySQL/app2`, and `/Test/EC2/Web/app1` will appear as:

```yaml
Dev:
  DBServer:
    MySQL:
      app1: <value>
      app2: <value>
  Web:
    app1: <value>
Test:
  DBServer:
    MySQL:
      app1: <value>
      app2: <value>
  Web:
    app1: <value>
```

Arguments can be passed too. Notably:

- `-f` Specify a yaml file to use (useful for multiple accounts/regions).
- `--engine` Specify a diff engine to use (defaults to DiffResolver).
- `--profile` Specify an AWS profile to use (defaults to profile).
- `--force` Used with the pull command. Overwrites parameters.yml file, keeping added keys.
