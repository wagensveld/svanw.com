---
title: "Hot Chocolate Mix"
description: "A simple hot chocolate mix."
summary: "A simple hot chocolate mix."
date: "2021-12-14T23:19:39"
category: "recipes"
series:
  - "drinks"
---

1. Combine equal parts by volume in a bowl.
   - **Sugar**
   - **Cocoa Powder**

I find for a mugs worth of hot chocolate, 1 Tbsp and coca powder, and 1Tbsp of sugar works nicely.
