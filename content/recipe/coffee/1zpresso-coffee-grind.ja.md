---
title: "1Zpresso JX/JX Proのコーヒー豆の挽き加減"
description: "より細かく研磨するには、ダイヤルまたは針を時計回りに回します。"
category: "food"
---

1Zpresso をどの方向に調整するか忘れがちなので参考ページです。

より細かく研磨するには、ダイヤルまたは針を時計回りに回します。

<!--more-->

グラインドアジャスターを直接見ていると仮定すると:

## JX

### より細かい

針(参照マーカーがある場所)を時計回りに回します(数字が大きくなります)。

### より粗い

針(参照マーカーがある場所)を反時計回りに回します(数字が小さくなります)。

## JX Pro

### より細かい

ダイヤル(数字がある場所)を時計回りに回します(数字が小さくなります)。

### より粗い

ダイヤル(数字がある場所)を反時計回りに回します(数字が大きくなります)。
