---
title: "Create a Jenkins Library"
description: "Create groovy files in a git repo under /vars/"
summary: "Create groovy files in a git repo under /vars/"
date: "2021-08-06T21:06:40"
series: ["devops"]
category: "guides"
---

The first step is to create a repository for your Jenkins library, if it's made private, make sure Jenkins has access to it.

[Here is a reference.](https://gitlab.com/wagensveld/jenkins-library)

Before we populate the repo, let's add it to Jenkins:
`Manage Jenkins` –> `Configure System`, find the `Global Pipeline Libraries` section, click `Add`.

The `Name` can be anything, it's what you'll refer to in the Jenkins pipeline, the default version is the branch/tag you want to use by default. Everything else can be left default.

The structure of the repo will be:

```
repository
|-resources
|-src
|-vars
```

Essentially `vars` are the functions you call within your pipeline, src are groovy source files you can use for more complex library functions, resources are static helper data for the src files.

For the sake of this we'll just be focused on vars.

Take this simple function:

{{< embed-remote-code "https://gitlab.com/wagensveld/jenkins-library/-/raw/master/vars/paramsDescription.groovy" >}}

Let's name it `paramsDescription.groovy` and put it in the vars repo.

Once that is pushed, in order to use it in a pipeline, just add this to the top of your Jenkinsfile `@Library('my-library') _`, `my-library` being the name you specified earlier, you don't need to do this if you set `Load implicitly` to `true` during setup.

You can also specify a specific branch/tag: `@Library('wagensveld-jenkins@feature/branch') _` or `@Library('wagensveld-jenkins@1.0') _`
