---
title: "Udon Noodles"
description: "Fairly versatile udon noodle recipe."
summary: "Fairly versatile udon noodle recipe."
date: "2022-09-26T19:37:16"
# images:
#  - "images/udon-noodles.webp"
series:
  - "udon"
category: "recipes"
---

> Makes 4 servings

1. Mix water salt and egg yolk in a jug.
   - **Water**, 220ml
   - **Salt**, 28g
   - **Egg Yolk**, 1
2. In a stand mixer with a paddle attachment combine flour and water mixture, until small clumps form.
   - **Bread Flour**, 500g
   - **Water Mixture**
3. Press with a ball and cover with cling wrap, seal and let rest for at least 2 hours at room temperature. Ideally if you want wait 8 or so hours, wait that long.
4. Roll dough out into workable sheets.
5. Use a pasta maker to knead the dough, go from the largest to the 2nd then 3rd setting. Then take the dough out fold it in half, and repeat until the sheet is smooth.
6. Cut noodles into desired thickness.
7. Bundle noodles into 4 equal portions.
8. Boil until desired consistency is reached, 6 or so minutes, strain.
