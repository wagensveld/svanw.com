---
title: "自動的にJenkins Jobsを生成します"
description: "Job DSLを使用して"
summary: "Job DSLを使用して"
date: "2021-08-06T21:22:18"
category: "guides"
---

これは自動的に JenkinsJobs を提供することができる方法です

JenkinsJob のリポジトリを持つ 1 つの方法は、[ジョブ DSL プラグイン](https://plugins.jenkins.io/job-dsl/)を使用することです。

[私のリポジトリをガイドとして使用してください。](https://gitlab.com/wagensveld/jenkins-jobs)

(以下は、そのリポジトリの Readme ファイルから主に取られます)

## 作り方

必要がある最初のものは `Job DSL`プラグインをインストールすることです:
`Manage Jenkins` -> `Manage Plugins` -> `Available`の管理をして、`Job DSL`プラグインを再起動せずにインストールします。

その後、 `New Item`を使用して DSL ジョブを作成する、名前と場所はあなた次第で、`FreeStyle Project`として作成します。

`Build`の下でこの新しく作成されたジョブでは、`Add build step` -> `Process Job DSLs`をクリックして、`Use the provided DSL script`を選択します。

以下のようなものを使ってください:

```groovy
pipelineJob('example-job-1') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-1/Jenkinsfile')
    }
  }
}

pipelineJob('example-job-2') {
  definition {
    cpsScm {
      scm {
        git('https://gitlab.com/wagensveld/jenkins-jobs.git', 'master') { node ->
          node / gitConfigName('DSL User')
          node / gitConfigEmail('me@me.com')
        }
      }
      scriptPath('jobs/example-job-2/Jenkinsfile')
    }
  }
}
```

`example-job-1`あなたが作りたい仕事の名前である、`git`リポジトリであること、`master`を指定する必要はありませんが、望むなら分岐を変更することができます。`gitconfig`の値は私が gitlab のためにやらなければならなかった回避策です、DSL は Github にとってはあまり友達になるようです。

Jenkins をきれいにするために、`Action for removed jobs/views/config files`を設定することをお勧めします。

`http://yourjenkinsurl.com/plugin/job-dsl/api-viewer/index.html`にアクセスした場合は、API リファレンスで迎えられます。ここでは、より複雑な構成を実行できます。

サブディレクトリの作成 `pipelinejob('subdirectory/example-job-2')`は最初に Jenkins にフォルダを作成する必要があります。

最後にこの変更のための実装は GIT サービスによって異なりますが、ビルドトリガーとして GIT プッシュを使用することは非常に便利です。
